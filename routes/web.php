<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', function(){ 
    return redirect()->route('dashboard'); 
});
Route::group(['middleware' => ['auth'], 'prefix' => 'dashboard' ], function(){

    Route::middleware('check-period')->group(function(){
        Route::get('/', 'HomeController@index')->name('dashboard');
        Route::resource('measures', 'MeasureController');
        Route::resource('presentations', 'PresentationController');
        Route::resource('items', 'ItemController');
    });
    
    Route::resource('periods', 'PeriodController');

});