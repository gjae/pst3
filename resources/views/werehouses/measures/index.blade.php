@extends('layouts.app')

@section('area-title', 'Unidades de medida')

@push('css')
    
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/datatables.min.css') }}">
@endpush

@section('content')
<div class="row">

    <div class="col-md-5 col-sm-12 col-lg-5 text-right offset-md-7 offset-lg-7">
        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#verifyModalContent" data-whatever="@create">
            Agregar
        </button>
    </div>

</div>
<div class="table-responsive">
    <table id="scroll_vertical_table" class="display table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Unidad</th>
                <th>Abreviatura</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($measures as $measure)
                <tr>
                    <td>{{ $measure->id }}</td>
                    <td>{{ $measure->measure }}</td>
                    <td>{{ $measure->abbr }}</td>
                    <td>
                        <button 
                            class="btn btn-success" 
                            data-toggle="modal" 
                            data-target="#verifyModalContent" 
                            data-whatever="@edit" 
                            data-measure="{{ $measure->measure }}" data-abbr="{{ $measure->abbr }}" data-measure-id="{{ $measure->id }}" 
                            data-update-url="{{ route('measures.update', $measure) }}" >
                            Editar
                        </button>
                        <button 
                            class="btn btn-danger delete-btn" 
                            data-measure="{{ $measure->measure }}" data-abbr="{{ $measure->abbr }}" data-measure-id="{{ $measure->id }}" 
                            data-update-url="{{ route('measures.update', $measure) }}" >
                            Eliminar
                        </button>
                        <form action="{{ route('measures.destroy', $measure) }}" id="delete-measure-{{ $measure->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Unidad</th>
                <th>Abreviatura</th>
                <th>Opciones</th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="modal fade" id="verifyModalContent" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="verifyModalContent_title">Nueva unidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('measures.store') }}" method="POST" id="measure-form">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="POST">
                    <div class="form-group">
                        <label for="recipient-name-2" class="col-form-label">Unidad</label>
                        <input type="text" placeholder="Unidad de medida" class="form-control" required name="measure" id="measure">
                    </div>
                    <div class="form-group">
                        <label for="message-text-1" class="col-form-label">Abreviatura</label>
                        <input type="text" name="abbr" required placeholder="Ejmplo: KG" class="form-control" id="abbr">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="save" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
    <script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- page vendor js -->
    <script src="{{ asset('assets/js/vendor/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatables.script.js') }}"></script>
    <script src="{{ asset('assets/js/es5/script.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/sidebar.large.script.min.js') }}"></script>

    <script src="{{ asset('assets/js/modal.script.js') }}"></script>
@endpush