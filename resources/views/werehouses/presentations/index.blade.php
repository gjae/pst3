@extends('layouts.app')

@section('area-title', 'Unidades de medida')

@push('css')
    
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/datatables.min.css') }}">
@endpush

@section('content')
<div class="row">

    <div class="col-md-5 col-sm-12 col-lg-5 text-right offset-md-7 offset-lg-7">
        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#presentationModalContent" data-whatever="@create">
            Agregar
        </button>
    </div>

</div>
<div class="table-responsive">
    <table id="scroll_vertical_table" class="display table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Presentacion</th>
                <th>Unidad</th>
                <th>Peso / empaque</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($presentations as $presentation)
                <tr>
                    <td>{{ $presentation->id }}</td>
                    <td>{{ $presentation->presentation }}</td>
                    <td>{{ $presentation->unit->measure }}</td>
                    <td>{{ $presentation->weight }}</td>
                    <th>{{ !is_null($presentation->discared_inventory_at) ? 'Fuera de inventario' : 'En inventario' }}</th>
                    <td>
                        <button 
                            class="btn btn-success" 
                            data-toggle="modal" 
                            data-target="#presentationModalContent" 
                            data-whatever="@edit"   
                            data-presentation="{{ $presentation->presentation }}" 
                            data-measure-id="{{ $presentation->measure_unit_id }}" 
                            data-presentation-id="{{ $presentation->id }}" 
                            data-update-url="{{ route('presentations.update', $presentation) }}" 
                            data-weight="{{ $presentation->weight }}">
                            Editar
                        </button>
                        <button 
                            class="btn btn-danger delete-presentation-btn" 
                            data-presentation="{{ $presentation->presentation }}" 
                            data-measure_id="{{ $presentation->measure_unit_id }}" 
                            data-presentation-id="{{ $presentation->id }}" 
                            data-update-url="{{ route('presentations.update', $presentation) }}" >
                            Eliminar
                        </button>
                        <form action="{{ route('presentations.destroy', $presentation) }}" id="delete-presentation-{{ $presentation->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Presentacion</th>
                <th>Unidad</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="modal fade" id="presentationModalContent" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="presentationModalContent_title">Nueva presentacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('presentations.store') }}" method="POST" id="presentation-form">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="presentation">Presentación</label>
                                    <input type="text" placeholder="Nombre de la presentación" required class="form-control validate" id="presentation" name="presentation" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="presentation">Peso - presentación</label>
                                    <input type="number" step=".1" placeholder="Nombre de la presentación" required class="form-control validate" id="weight" name="weight" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Unidad de medida</label>
                        <select name="measure_unit_id" id="measure_unit_id" class="form-control" required>
                            <option value="">Elija uno</option>
                            @foreach ($measures as $measure)
                                <option value="{{ $measure->id }}">
                                    {{ $measure->measure }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="save-presentation" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
    <script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- page vendor js -->
    <script src="{{ asset('assets/js/vendor/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/script.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/sidebar.large.script.min.js') }}"></script>

    <script src="{{ asset('assets/js/modal.script.js') }}"></script>
    <script src="{{ asset('assets/js/datatables.script.js') }}"></script>
@endpush