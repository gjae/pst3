@extends('layouts.app')

@section('area-title', 'Unidades de medida')

@push('css')
    
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/datatables.min.css') }}">
@endpush

@section('content')
<div class="row">

    <div class="col-md-5 col-sm-12 col-lg-5 text-right offset-md-7 offset-lg-7">
        <button type="button" class="btn btn-primary m-1" data-toggle="modal" data-target="#itemModalContent" data-whatever="@create">
            Agregar
        </button>
    </div>

</div>
<div class="table-responsive">
    <table id="scroll_vertical_table" class="display table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Codigo</th>
                <th>Item</th>
                <th>Presentciones</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->item }}</td>
                    <td>{{ $item->item_code }}</td>
                    <td>
                        @foreach ($item->presentations as $presentation)
                            <span class="badge badge-pill badge-outline-primary p-2 m-1">
                                    {{ $presentation->presentation->presentation }} de {{ $presentation->presentation->weight }} {{ $presentation->presentation->unit->measure }}
                            </span>
                            
                        @endforeach
                    </td>
                    <td>
                        <a 
                            class="btn btn-success" 
                            href="{{ route('items.edit', $item) }}"
                        >
                            Editar
                        </a>
                        <button 
                            class="btn btn-danger delete-btn"  
                            data-measure-id="{{ $item->id }}"    
                        >
                            Eliminar
                        </button>
                        <form action="{{ route('items.destroy', $item) }}" id="delete-measure-{{ $item->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Codigo</th>
                <th>Item</th>
                <th>Presentciones</th>
                <th>Opciones</th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="modal fade" id="itemModalContent" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemModalContent_title">Nuevo item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('items.store') }}" method="POST" id="measure-form">
                    @csrf
                    <input type="hidden" name="_method" id="_method" value="POST">
                    <div class="form-group">
                        <label for="recipient-name-2" class="col-form-label">Item</label>
                        <input type="text" placeholder="Unidad del" class="form-control" required name="item" id="item">
                    </div>
                    <div class="form-group">
                        <label for="message-text-1" class="col-form-label">Codigo</label>
                        <input type="text" name="item_code" required placeholder="Codigo de barra / identificación" class="form-control" id="item_code">
                    </div>
                    <div class="form-group">
                        <label for="presentations">Presentaciones / empaques</label>
                        <select name="presentations[]" id="presentations" required multiple class="form-control">
                            @foreach ($presentations as $presentation)
                                <option value="{{ $presentation->id }}">{{ $presentation->presentation }} / {{ $presentation->weight }} {{ $presentation->unit->measure }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="save" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
    <script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- page vendor js -->
    <script src="{{ asset('assets/js/vendor/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatables.script.js') }}"></script>
    <script src="{{ asset('assets/js/es5/script.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/sidebar.large.script.min.js') }}"></script>

    <script src="{{ asset('assets/js/modal.script.js') }}"></script>
@endpush