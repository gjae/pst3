@extends('layouts.app')

@section('area-title', 'Unidades de medida')

@push('css')
    
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/datatables.min.css') }}">
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        <form action="{{ route('items.update', $item) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="col-12">
                <label for="" class="control-label">Item</label>
                <input type="text" value="{{ $item->item }}" placeholder="Nombre del item" class="form-control" name="item" required="required">
            </div>
            <div class="col-12">
                <label for="" class="control-label">Codigo del item</label>
                <input type="text" value="{{ $item->item_code }}" class="form-control" name="item_code" id="item_code">
            </div>
            <div class="col-12">
                <label for="">Presentaciones</label>
                <select name="presentations[]" id="presentations" multiple class="form-control" required="required">
                    @foreach ($presentations as $presentation)
                        <option {{ is_bool( $item->hasPresentation($presentation->id) ) ? 'selected' : '' }} value="{{ $presentation->id }}">
                            {{ $presentation->presentation }} / {{ $presentation->weight }}  {{ $presentation->unit->measure }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="row mt-2">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success btn-block">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@push('js')
    <script src="{{ asset('assets/js/vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- page vendor js -->
    <script src="{{ asset('assets/js/vendor/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatables.script.js') }}"></script>
    <script src="{{ asset('assets/js/es5/script.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/sidebar.large.script.min.js') }}"></script>

    <script src="{{ asset('assets/js/modal.script.js') }}"></script>
@endpush