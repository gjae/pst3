<div class="side-content-wrap">
    <div class="sidebar-left {{ resolve('Period\Provider')['hasActivePeriod'] ? 'open': '' }} rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item active" data-item="sessions">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Administrator"></i>
                    <span class="nav-text">Usuarios</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item" data-item="forms">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-File-Clipboard-File--Text"></i>
                    <span class="nav-text">Institución</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item" data-item="uikits">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Library"></i>
                    <span class="nav-text">Distribución</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item " data-item="dashboard">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Inventario</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item" data-item="extrakits">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Suitcase"></i>
                    <span class="nav-text">Extra kits</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item" data-item="apps">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Computer-Secure"></i>
                    <span class="nav-text">Apps</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item">
                <a class="nav-item-hold" href="datatables.html">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="nav-text">Datatables</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item" data-item="others">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Double-Tap"></i>
                    <span class="nav-text">Others</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item">
                <a class="nav-item-hold" href="http://demos.ui-lib.com/gull-html-doc/" target="_blank">
                    <i class="nav-icon i-Safe-Box1"></i>
                    <span class="nav-text">Doc</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="dashboard">
            <li class="nav-item">
                <a href="{{ route('measures.index') }}" class="open">
                    <i class="nav-icon i-Clock-3"></i>
                    <span class="item-name">Unidades de medida</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('presentations.index') }}">
                    <i class="nav-icon i-Clock-4"></i>
                    <span class="item-name">Presentaciones</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('items.index') }}">
                    <i class="nav-icon i-Over-Time"></i>
                    <span class="item-name">Items</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="dashboard.v4.html">
                    <i class="nav-icon i-Clock"></i>
                    <span class="item-name"></span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="forms">
            <li class="nav-item">
                <a href="form.basic.html">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                    <span class="item-name">Institución</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="form.layouts.html">
                    <i class="nav-icon i-Split-Vertical"></i>
                    <span class="item-name">Periodos</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="apps">
            <li class="nav-item">
                <a href="invoice.html">
                    <i class="nav-icon i-Add-File"></i>
                    <span class="item-name">Invoice</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="inbox.html">
                    <i class="nav-icon i-Email"></i>
                    <span class="item-name">Inbox</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="chat.html">
                    <i class="nav-icon i-Speach-Bubble-3"></i>
                    <span class="item-name">Chat</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="extrakits">
            <li class="nav-item">
                <a href="image.cropper.html">
                    <i class="nav-icon i-Crop-2"></i>
                    <span class="item-name">Image Cropper</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="loaders.html">
                    <i class="nav-icon i-Loading-3"></i>
                    <span class="item-name">Loaders</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="ladda.button.html">
                    <i class="nav-icon i-Loading-2"></i>
                    <span class="item-name">Ladda Buttons</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="toastr.html">
                    <i class="nav-icon i-Bell"></i>
                    <span class="item-name">Toastr</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="sweet.alerts.html">
                    <i class="nav-icon i-Approved-Window"></i>
                    <span class="item-name">Sweet Alerts</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="tour.html">
                    <i class="nav-icon i-Plane"></i>
                    <span class="item-name">User Tour</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="upload.html">
                    <i class="nav-icon i-Data-Upload"></i>
                    <span class="item-name">Upload</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="uikits">
            <li class="nav-item">
                <a href="alerts.html">
                    <i class="nav-icon i-Bell1"></i>
                    <span class="item-name">Centros de acopio</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="accordion.html">
                    <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                    <span class="item-name">Planteles</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="signin.html">
                    <i class="nav-icon i-Checked-User"></i>
                    <span class="item-name">Usuarios registrados</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="others">
            <li class="nav-item">
                <a href="not.found.html">
                    <i class="nav-icon i-Error-404-Window"></i>
                    <span class="item-name">Not Found</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="user.profile.html">
                    <i class="nav-icon i-Male"></i>
                    <span class="item-name">User Profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="blank.html">
                    <i class="nav-icon i-File-Horizontal"></i>
                    <span class="item-name">Blank Page</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>