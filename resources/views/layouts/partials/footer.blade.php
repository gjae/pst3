
<div class="app-footer">
    <div class="row">
        <div class="col-md-9">
            <p>
                <strong>
                    Desarrollado como software libre bajo la licencia <a href="https://opensource.org/licenses/MIT">MIT</a>
                </strong>
            </p>
        </div>
    </div>
</div>