@extends('layouts.app')

@section('area-title', 'Nuevo periodo')

@push('css')
    
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/pickadate/classic.css') }}">
<link rel="stylesheet" href="{{ asset('assets/styles/vendor/pickadate/classic.date.css') }}">
@endpush

@section('content')

<form action="{{ route('periods.store') }}" method="post">
    @csrf

    <div class="rrow">
        <div class="col-md-3 form-group mb-3 offset-md-4">
            <label for="firstName1">Periodo</label>
            <input type="text" required class="form-control" id="period" name="period" placeholder="Nombre del periodo">
        </div>
        <div class="col-md-3  form-group mb-3 offset-md-4">
            <label class="switch pr-5 switch-primary mr-3">
                <span>Periodo especial</span>
                <input type="checkbox" name="is_special_period">
                <span class="slider"></span>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 form-group mb-3">
            <label for="picker2">Inicia</label>
            <input id="openned_at" required name="openned_at" class="form-control date-picker" placeholder="yyyy-mm-dd">
        </div>
        <div class="col-md-3 form-group mb-3">
            <label for="picker2">Cierra</label>
            <input id="closed_at" required name="closed_at" class="form-control date-picker" placeholder="yyyy-mm-dd">
        </div>
        <div class="col-md-3 form-group mb-3">
            <label for="picker2">Maximo tiempo de carga hasta</label>
            <input id="loadable_data_unil" required name="loadable_data_unil" class="form-control date-picker" placeholder="yyyy-mm-dd">
        </div>
        <div class="col-md-3 form-group mb-3">
            <label for="picker2">Se puede cargar facturas hasta</label>
            <input id="loadable_invoices_until" required name="loadable_invoices_until" class="form-control date-picker" placeholder="yyyy-mm-dd">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success btn-block">Guardar</button>
        </div>
    </div>
</form>

@endsection

@push('js')
<script src="{{ asset('assets/js/vendor/pickadate/picker.js') }}"></script>
<script src="{{ asset('assets/js/vendor/pickadate/picker.date.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.date-picker').pickadate();
    });
</script>
@endpush
