$(document).ready(function() {
    var store_url = $("#measure-form").attr('action');
    var presentation_url_store = $("#presentation-form").attr('action');


    $('#verifyModalContent').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            
        var modal = $(this)
        if( recipient != "@create" ){
            var url = button.data('update-url')
            let form = $("#measure-form");
            form.attr('action', url);
            $("#_method").val('PUT')
            $("#measure").val( button.data('measure') );
            $("#abbr").val( button.data('abbr') );
            $("#verifyModalContent_title").text("Editar unidad de medida");
        }
    })

    $("#save").on('click', e => {
        var form = $("#measure-form");
        form.submit();
    });

    $("#verifyModalContent").on('hide.bs.modal', e => {
        let form = $("#measure-form");
        form.attr('action', store_url);
        $("#_method").val('POST')
        $("#measure").val("");
        $("#abbr").val("");
        $("#wiehgt").val(0.0)
        $("#verifyModalContent_title").text("Nueva unidad");
    })


    $(".delete-btn").on('click', e => {
        var button = e.target;
        if( confirm("¿Seguro que desea realizar esta acción?") )
            $("#delete-measure-"+ button.getAttribute('data-measure-id')).submit();
    })


    /**
     * Presentaciones
     */
    $('#presentationModalContent').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            
        var modal = $(this)
        if( recipient != "@create" ){
            var url = button.data('update-url')
            let form = $("#presentation-form");
            form.attr('action', url);
            $("#_method").val('PUT')
            $("#presentation").val( button.data('presentation') );
            $(`#measure_unit_id option:eq(${ button.data('measure-id') })`).prop('selected', true)
            $("#weight").val( button.data('weight') )
            $("#presentationModalContent_title").text("Editar presentación");
        }
    })

    $("#presentationModalContent").on('hide.bs.modal', e => {
        let form = $("#presentation-form");
        form.attr('action', presentation_url_store);
        $("#_method").val('POST')
        $("#presentation").val("");
        $(`#measure_unit_id option:eq(${''})`).prop("selected", false);
        $("#weight").val(0.00)
        $("#presentationModalContent_title").text("Nueva presentacion");
    })

    $("#save-presentation").on('click', e => {
        var form = $("#presentation-form");
        form.submit();
    });

    $(".delete-presentation-btn").on('click', e => {
        var button = e.target;
        if( confirm("¿Seguro que desea realizar esta acción?") )
            $("#delete-presentation-"+ button.getAttribute('data-presentation-id')).submit();
    })
})