<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentation extends Model
{
    protected $fillable = [
        'presentation', 'measure_unit_id', 'institution_id', 'discared_inventory_at', 'weight'
    ];

    public function unit()
    {
        return $this->belongsTo(\App\MeasureUnit::class, 'measure_unit_id');
    }

    public function institution()
    {
        return $this->belongsTo(\App\Institution::class ,'institution_id');
    }

    public function items()
    {
        return $this->hasMany(\App\ItemPresentation::class , 'presentation_id');
    }

}
