<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    protected $fillable = [
        'collection_center_id', 'receiver', 'address', 'phone', 'coords', 'dispatch_frequency'
    ];

    public function gatherings()
    {
        return $this->belongsTo(\App\CollectionCenter::class);
    }
}
