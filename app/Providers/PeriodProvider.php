<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Period;
use Auth;
class PeriodProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Period\Provider', function($app){
            
            $user = Auth::user();

            $withPeriod = [ 'hasActivePeriod' => $user->institution->hasActivePeriod(), 'current_period' => $user->institution->latestPeriod() ];

            return $withPeriod;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
