<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPresentation extends Model
{
    protected $fillable = [
        'item_id', 'presentation_id', 'available', 'dispatched', 'has_unities', 'unities'
    ];


    public function item()
    {
        return $this->belongsTo(\App\Item::class);
    }

    public function presentation()
    {
        return $this->belongsTo(\App\Presentation::class);
    }

    public function dispatches()
    {
        return $this->hasMany(\App\DispatchDetail::class, 'item_presentation_id');
    }
}
