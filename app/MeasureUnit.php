<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasureUnit extends Model
{
    protected $fillable = [
        'measure', 'abbr'
    ];

    public function presentations()
    {
        return $this->hasMany(\App\Presentation::class, 'measure_unit_id');
    }

}
