<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstitutionUser extends Model
{
    protected $fillable = [
        'user_id', 'institution_id'
    ];

    public function creator()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function institution()
    {
        return $this->belongsTo(\App\Institution::class);
    }
    
}
