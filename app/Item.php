<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ItemPresentation;

class Item extends Model
{

    use SoftDeletes;
    protected $fillable = [
        'item', 'item_code'
    ];

    public function presentations()
    {
        return $this->hasMany(\App\ItemPresentation::class);
    }


    public function syncPresentations(array $presentations)
    {
        $this->presentations->each(function($presentation){
            if( $presentation->dispatches->isEmpty() ) $presentation->delete();
        });

        collect( $presentations )->each(function($item, $index){
            $this->presentations()->save(
                new ItemPresentation([ 'presentation_id' => $item ])
            );
        });


        return $this;
    }
    
    public function hasPresentation(int $id)
    {
        $has = $this->presentations->search(function($presentation) use(&$id){
            return $presentation->presentation_id == $id;
        });

        return $has;
    }


    public function hasDispatches()
    {
        $has = $this->presentations->search( function($presentation){
            return !$presentation->dispatches->isEmpty();
        });


        return $has;
    }
}
