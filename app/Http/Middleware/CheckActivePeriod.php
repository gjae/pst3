<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use App\Period;
class CheckActivePeriod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $period = resolve('Period\Provider');
        if( !$period['hasActivePeriod'] )
            return redirect()->route('periods.create');


        return $next($request);
    }
}
