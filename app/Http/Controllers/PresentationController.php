<?php

namespace App\Http\Controllers;

use App\Presentation;
use App\MeasureUnit;
use Illuminate\Http\Request;

use Validator;
class PresentationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presentations = Presentation::all();
        $measures      = MeasureUnit::all();
        return view('werehouses.presentations.index', compact('presentations', 'measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'presentation'      => 'required',
            'measure_unit_id'   => 'required'
        ], [
            'presentation.required'     => 'Debe agregar una descripción de la presentación',
            'measure_unit_id.required'   => 'Debe seleccionar una unidad de medida'            
        ]);

        if( $validation->fails() ) return redirect()->back()->withErrors($validation->errors());

        $request->request->add([
            'institution_id'    => auth()->user()->institution->id
        ]);

        Presentation::create($request->all());

        return redirect()->route('presentations.index')->with('success', 'El registro ha sido agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Presentation  $presentation
     * @return \Illuminate\Http\Response
     */
    public function show(Presentation $presentation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Presentation  $presentation
     * @return \Illuminate\Http\Response
     */
    public function edit(Presentation $presentation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Presentation  $presentation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'presentation'      => 'required',
            'measure_unit_id'   => 'required'
        ], [
            'presentation.required'     => 'Debe agregar una descripción de la presentación',
            'measure_unit_id.required'   => 'Debe seleccionar una unidad de medida'            
        ]);

        if( $validation->fails() ) return redirect()->back()->withErrors($validation->errors());
        
        $presentation = Presentation::findOrFail( $id );
        $presentation->fill($request->except(['_method', '_token']));
        $presentation->save();

        return \redirect()->route('presentations.index')->with('success', 'El registro ha sido actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Presentation  $presentation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $presentation = Presentation::findOrFail($id);
        if( !$presentation->items->isEmpty() )
            return redirect()->back()->withErrors([
                'presentation'  => [
                    'No se puede eliminar este registro, ya posee items relacionados'
                ]
            ]);

        $presentation->delete();
        return \redirect()->back()->with('success', 'Se ha eliminado el registro correctamente');
        
    }
}
