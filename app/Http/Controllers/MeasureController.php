<?php

namespace App\Http\Controllers;

use App\MeasureUnit;
use Illuminate\Http\Request;

use Illuminate\Http\JsonResponse;
class MeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measures = MeasureUnit::all();

        return view('werehouses.measures.index', compact('measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'institution_id'    => auth()->user()->institution->id
        ]);
        MeasureUnit::create($request->all());
        return redirect()->route('measures.index')->with('success', 'La nueva unidad de medida ha sido registrada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MeasureUnit  $measureUnit
     * @return \Illuminate\Http\Response
     */
    public function show(MeasureUnit $measureUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MeasureUnit  $measureUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(MeasureUnit $measureUnit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MeasureUnit  $measureUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $measureUnit = MeasureUnit::find($id);
        $measureUnit->fill( $request->except(['_token', '_method']) );
        $measureUnit->save();
        return redirect()->route('measures.index')->with('success', 'Datos de la unidad de medida modificados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MeasureUnit  $measureUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $measureUnit = MeasureUnit::find($id);
        if( $measureUnit->presentations->isEmpty() )
        {
            $measureUnit->delete();
            return redirect()->route('measures.index')->with('success', 'El registro ha sido eliminado correctamente');
        }

        return redirect()->route('measures.index')->withErrors([
            'measure' => [ 
                'No es posible eliminar una unidad de medida con registros asociados'
            ] 
        ]);
    }
}
