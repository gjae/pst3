<?php

namespace App\Http\Controllers;

use App\Item;
use App\Presentation;
use Illuminate\Http\Request;
use DB;
use Validator;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        $presentations = Presentation::all();
        return view('werehouses.items.index', compact('items', 'presentations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'item_code'     => 'required|unique:items,item_code',
            'item'          => 'required',
            'presentations' => 'required|array'
        ], [
            'item_code.required'    => 'Codigo de item no ingresado o incorrecto',
            'item_code.unique'      => 'El codigo que trata de ingresar para el item, ya existe',
            'presentations.required'=> 'El item debe contener presentacions / empaques en los que sera almacenado',
            'item.required'         => 'El item debe contener un nombre valido'
        ]);

        if( $validation->fails() ) return redirect()->back()->withErrors($validation->errors());

        $endTransaction = DB::transaction(function() use(&$request){
            $item = Item::create( $request->except(['presentations']) );
            
            if( $request->has('presentations') )
                $item->syncPresentations( $request->presentations );
            
            return $item;
        });


        return redirect()->back()->with('success', 'El nuevo item ha sido agregado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $presentations = Presentation::all();
        return view('werehouses.items.edit', compact('presentations', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {

        return DB::transaction(function() use(&$item, &$request){

            if( $request->item_code != $item->item_code )
            {
                $existCode = Item::whereItemCode( $request->item_code )->first();
                if( !is_null( $existCode ) ) return redirect()->back()->withErrors([
                    'item_code' => [
                        'El codigo que intenta agregar al item ya esta siendo usado por otro, no puede haber dualidad'
                    ]
                ]);
            }
    
            $item->fill($request->only(['item', 'item_code']));
            $item->save();
            $item->syncPresentations( $request->presentations );

            return redirect()->route('items.index')->with('success', 'Datos del item actualizados correctamente');
        });

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if( !is_bool( $item->hasDispatches() ) ) 
            return redirect()->route('items.index')->withErrors([
                'item_dispatches'   => [
                    'No se puede eliminar este item, ya posee un historial de despachos'
                ]
            ]);
        
        $item->delete();
        return redirect()->route('items.index')->with('success', 'El item ha sido removido del sistema correctamente');
    }
}
