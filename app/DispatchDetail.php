<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DispatchDetail extends Model
{
    protected $fillable = [
        'dispatch_id', 'item_presentation_id', 'quantity', 'after_dispatch_qtty', 'before_dispatch_qtty'
    ];

    public function dispatch()
    {
        return $this->belongsTo(\App\Dispatch::class);
    }
}
