<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionCenter extends Model
{
    protected $fillable = [
        'collection_center',  'phone', 'institution_id', 'municipality_id', 'address', 'coords'
    ];

    public function institution()
    {
        return $this->belongsTo(\App\Institution::class);
    }

    public function municipality()
    {
        return $this->belongsTo(\App\Municipality::class);
    }
}
