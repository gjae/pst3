<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Spatie\Permission\Traits\HasRoles;

use App\Institution;
use App\InstitutionUser;
class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function dispatchs()
    {
        return $this->morphMany(\App\Dispatch::class, 'dispatchable');
    }

    public function receiveds()
    {
        return $this->morphMany(\App\Dispatch::class, 'receivable');
    }

    public function getNamespace()
    {
        return 'App\User';
    }

    public function institution()
    {
        return $this->hasOneThrough(
            \App\Institution::class, 
            \App\InstitutionUser::class, 
            'user_id', 
            'id', 
            'id', 
            'institution_id'
        );
    }


    public function syncInstitution(Institution $inst)
    {
        if( is_null($this->institution) )
            return InstitutionUser::create([ 'user_id' => $this->id, 'institution_id' => $inst->id ]);

        return $this->institution;
    }
}
