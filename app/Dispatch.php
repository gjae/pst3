<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Ramsey\Uuid\Uuid;
class Dispatch extends Model
{
    protected $fillable = [
        'dispatch_at', 'received_at','is_canceled', 'dispatch_uuid', 'dispatchable_type', 'dispatchable_id', 'receivable_type', 'receivable_id'
    ];

    protected $attributes = [
        'dispatch_uuid'    => '?.?'
    ];

    public function setDispatchUuidAttribute($old)
    {
        $this->attributes['dispatch_uuid'] (Uuid::uuid3(Uuid::NAMESPACE_DNS, env('APP_NAME', '--') ))->toString();
    }

    public function dispatcher()
    {
        return $this->morphTo('dispatchable');
    }

    public function received()
    {
        return $this->morphTo('receivable');
    }

    public function details()
    {
        return $this->hasMany(\App\DispatchDetail::class);
    }
}
