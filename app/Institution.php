<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Institution;
use App\InstitutionUser;
class Institution extends Model
{
    protected $fillable = [
        'user_id', 'name', 'address', 'phone', 'email'
    ];

    public function periods()
    {
        return $this->hasMany(\App\Period::class, 'institution_id');
    }

    public function hasActivePeriod()
    {
        return !is_null( $this->periods()->whereNull('closed_at')->first() );
    }

    public function latestPeriod()
    {
        return $this->periods()->whereNull('closed_at')->first() ;   
    }

    public function items_presentations()
    {
        return $this->hasMany(\App\Presentation::class, 'institution_id');
    }

    public function gatherines()
    {
        return $this->hasMany(\App\CollectionCenter::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function users()
    {
        return $this->hasManyThrough(
            \App\User::class,
            \App\InstitutionUser::class,
            'institution_id',
            'id',
            'id',
            'user_id',
        );    
    }


}
