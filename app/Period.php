<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
class Period extends Model
{
    protected $fillable = [
        'institution_id', 'period', 'openned_at', 'closed_at', 'loadable_data_unil', 'loadable_invoices_until', 'is_special_period'
    ];



    public function institution()
    {
        return $this->belongs(\App\Institution::class, 'institution_id');
    }

    public function setClosedAtAttribute($old)
    {   
        $this->attributes['openned_at'] = $this->parseFormat($old);
    }

    public function setOpennedAtAttribute($old)
    {
        $this->attributes['openned_at'] = $this->parseFormat($old);
    }

    public function setLoadableDataUnilAttribute($old)
    {
        $this->attributes['loadable_data_unil'] = $this->parseFormat($old);
    }

    public function setLoadableInvoicesUntilAttribute($old)
    {
        $this->attributes['loadable_invoices_until'] = $this->parseFormat($old);
    }


    public static function parseFormat($date)
    {
        try
        {
            return Carbon::parse($date);
        }catch(\Exception $e)
        {
            return dd($date);
        }
    }
}
