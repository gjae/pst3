<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### Desarrollado por 

- Giovanny Avila
- Sandino Chirinos
- Jose Luis Nuñez
- Jordan Briceño
- Diego Gallardo
- Betsi Parra

### Sección 33OA

### Titulo del proyecto: Aplicación web de monitoreo para la distribución de los rublos despachado por la Corporación Nacional de Alimentación Escolar CNAE S.A.

### Desarrollado bajo Laravel PHP Web Framework
## [Laravel](https://laravel.com/)

# Instrucciones de instalación

> git clone https://gitlab.com/gjae/pst3.git && cd pst3 && composer install && cp .env.example .env

# Ejecutar la base de datos

> php artisan migrate

# División logica basado en el patron MVVM / MVC

[Archivos de la base de datos: database/migrations](https://gitlab.com/gjae/pst3/tree/master/database/migrations)
[Archivos de vistas e interfaz de usuario: resources/views](https://gitlab.com/gjae/pst3/tree/master/resources)
[Archivos de controladores (procesos): app/Http/Controllers](https://gitlab.com/gjae/pst3/tree/master/app/Http)

# Terminología usada:

### Migracion (Desde el punto de vista de una base de datos)
- Archivos escritos en un lenguaje de programación que se exportan en formato de la BD, independientemente del motor que se use (mysql, postgresql, etc), el marco de trabajo se encarga de traducir cada [archivo de migracion](https://gitlab.com/gjae/pst3/tree/master/database/migrations) a su equivalente en SQL respectivo para cada motor, haciendo el desarrollo agnostico con respecto a la persistencia de datos.

>> Las migraciones son como el control de versiones de su base de datos, lo que permite a su equipo modificar y compartir fácilmente el  esquema de la base de datos de la aplicación. Las migraciones generalmente se combinan con el generador de esquemas de Laravel para construir fácilmente el esquema de la base de datos de su aplicación. Si alguna vez ha tenido que decirle a un compañero de equipo que agregue manualmente una columna a su esquema de base de datos local, se enfrenta al problema que resuelven las migraciones de la base de datos. La fachada del esquema de Laravel proporciona soporte agnóstico de base de datos para crear y manipular tablas en todos los sistemas de bases de datos compatibles de Laravel. - [Laravel Documentation](https://laravel.com/docs/6.x/migrations)

### Controladores
- Codigo escrito para manejar las peticiones y el procesamiento de datos entre las interfaces (usuarios o APIs) y el software propiamente. [Laravel Documentation - Controllers](https://laravel.com/docs/6.x/controllers#introduction)

### Modelos
- Un modelo es la capa de abstracción intermedia entre la aplicación y la persistencia de los datos, al igual que las migraciones es una manera de separar la logica de negocios con la fuente de los datos, al mismo tiempo un modelo es una [CLASE](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos) que representa un registro de una tabla especifica en la BD

### Ruta
- Una ruta es un punto de acceso a un recurso de un sistema

### Middleware
- Un [MIDDLEWAER](https://es.wikipedia.org/wiki/Middleware) es un punto intermedio dentro de la aplicación entre cualquier punto de la aplicación y la respuesta de la solicitud 
>> El middleware proporciona un mecanismo conveniente para filtrar las solicitudes HTTP que ingresan a su aplicación. Por ejemplo, Laravel incluye un middleware que verifica que el usuario de su aplicación esté autenticado. Si el usuario no está autenticado, el middleware lo redireccionará a la pantalla de inicio de sesión. Sin embargo, si el usuario está autenticado, el middleware permitirá que la solicitud continúe en la aplicación. [Laravel Middleware Documentation](https://laravel.com/docs/6.x/middleware)


# Ciclo de vida de la interacción de la aplicacion (ciclo de una peticion)
![alt text][logo]

[logo]: https://i0.wp.com/blog.mallow-tech.com/wp-content/uploads/2016/06/Laravel-Request-Life-Cycle.png?resize=1024%2C559 "Logo Title Text 2"

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
