<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->dateTime('dispatch_at')->nullable();
            $table->dateTime('received_at')->nullable();
            $table->boolean('is_canceled')->default(false);

            $table->string('dispatch_uuid', 34)->index();
            $table->string('dispatchable_type', 70)->default('--')->index();
            $table->string('receivable_type', 50)->default('--')->index();
            $table->unsignedBigInteger('receivable_id')->nullable();
            $table->unsignedBigInteger('dispatchable_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatches');
    }
}
