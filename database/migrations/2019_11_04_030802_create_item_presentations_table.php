<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemPresentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_presentations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('presentation_id');
            $table->decimal('available', 20,2)->default(0.00);
            $table->decimal('dispatched', 20,2)->default(0.00);

            $table->boolean('has_unities')->default(false);
            $table->integer('unities')->default(0);

            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('presentation_id')->references('id')->on('presentations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_presentations');
    }
}
