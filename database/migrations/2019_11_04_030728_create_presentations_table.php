<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presentations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('presentation', 100);
            $table->unsignedBigInteger('measure_unit_id');
            $table->unsignedBigInteger('institution_id');
            $table->dateTime('discared_inventory_at')->nullable();
            $table->decimal('weight', 12,2)->default(0.00);

            $table->foreign('measure_unit_id')->references('id')->on('measure_units')->onDelete('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presentations');
    }
}
