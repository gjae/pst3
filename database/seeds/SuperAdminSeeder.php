<?php

use Illuminate\Database\Seeder;

use App\User;
class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        
        $user = User::firstOr(function(){
            return User::create([
                'email' => 'admin@admin.com',
                'password'  => bcrypt('admin/2019'),
                'name'  => 'System Admin'
            ]);
        });
        $user->assignRole('Super Admin');
    }
}
